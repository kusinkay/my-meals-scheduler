��    *      l  ;   �      �  
   �     �     �     �     �     �                 -   8     f  '   �     �     �     �     �     �     �     �     �     �       #        =     E     S     e     q     z     �     �     �     �  	   �  
   �  [   �  I   $     n     �     �     �  y  �          -     @     U     f     {     �     �  ;   �  6   �  #   	  6   ?	     v	     �	     �	     �	     �	     �	     �	     �	     �	     �	  +   �	  
   )
     4
     A
     T
     c
     k
     s
     z
     �
     �
  
   �
     �
  k   �
  L   '  	   t     ~     �     �           *   
                 	                    $                               )                (   %   #             "       '                                 !   &                                      Add course Available dishes By cooking type By day type By dinner guest type By dish order By meal Change meal Clipboard copy is not available Copied ingredients to clipboard text-formated Copy ingredients to clipboard Could not copy ingredients to clipboard Create meal Devel options Dinner Friday Ingredients JavaScript disabled Lunch Meal Monday My Meals Scheduler Options for developers and debugers Refresh Remove course Remove ingredient Remove meal Saturday Sunday Thursday Tuesday Turn it holiday Turn it work day Wednesday Week Meals WordPress plugin to generate a week meals table based on the dishes that work on our family You need to enable JavaScript in your browser so you can use '%s' system. for all guest types main courses sole courses starters Project-Id-Version: My Meals Scheduler 0.0.0_BETA
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/my-meals-scheduler
PO-Revision-Date: 2022-03-06 18:47+0100
Last-Translator: 
Language-Team: 
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n != 1);
 Añadir plato Platos disponibles Por tipo de cocción Por tipo de día Por tipo de comensal Por orden de plato Por comidas Cambiar menú La copia al portapapeles no está disponible en tu navgador Ingredientes copiados al portapapeles en formato texto Copiar ingredientes al portapapeles No ha sido posible copiar ingredientes al portapapeles Crear menú Opciones de desarrollo Cena Viernes Ingredientes JavaScript desactivado Almuerzo Menú Lunes Planificador semanal de menus Opciones para desarrolladores y depuración Actualizar Borrar plato Borrar ingrediente Eliminar menú Sábado Domingo Jueves Martes Hazlo festivo Hazlo laborable Miércoles Menús semanales Plugin de Wordpress para generar los menus semanales sobre platos que funcionan en nuestro entorno familiar Deberías activar el JavaScript en tu navegador para usar el sistema de '%s' populares segundos únicos primeros 