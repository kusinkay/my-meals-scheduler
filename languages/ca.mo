��    (      \  5   �      p  
   q     |     �     �     �     �     �     �     �  -         .  '   L     t     �     �     �     �     �     �     �     �  #   �     �     �               &     /     6     ?     G     W  	   h  
   r  I   }     �     �     �     �  y  �     x     �     �     �     �     �  	   �     �  B   �  3   ?  #   s  7   �     �     �     �  	   �     	     	     (	     .	     4	  (   <	  	   e	     o	     {	     �	     �	     �	     �	     �	     �	     �	     �	     �	  T   �	     M
     V
     ]
     d
                                       $                	               '          %               
      #                               &                    "          !          (    Add course Available dishes By cooking type By day type By dinner guest type By dish order By meal Change meal Clipboard copy is not available Copied ingredients to clipboard text-formated Copy ingredients to clipboard Could not copy ingredients to clipboard Create meal Devel options Dinner Friday Ingredients JavaScript disabled Lunch Meal Monday Options for developers and debugers Refresh Remove course Remove ingredient Remove meal Saturday Sunday Thursday Tuesday Turn it holiday Turn it work day Wednesday Week Meals You need to enable JavaScript in your browser so you can use '%s' system. for all guest types main courses sole courses starters Project-Id-Version: My Meals Scheduler 0.0.0_BETA
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/my-meals-scheduler
PO-Revision-Date: 2022-03-06 18:47+0100
Last-Translator: 
Language-Team: 
Language: ca_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 2.0.6
Plural-Forms: nplurals=2; plural=(n != 1);
 Afegir plat Plats disponibles Per tipus de cocció Per tipus de dia Per tipus de comensat Per ordre de plat Per àpat Canviar àpat La còpia al porta-retalls no està disponible en el teu navegador Ingredients copiats al porta-retalls en format text Copiar ingredients al porta-retalls No ha esta possible copiar ingredients al porta-retalls Crear àpat Opcions de desenvolupament Sopar Divendres Ingredients JavaScript desactivat Dinar Àpat Dilluns Opcions per desenvolupament i depuració Recarrega Treure plat Esborrar Ingredient Esborrar àpat Dissabte Diumenge Dijous Dimarts Fes-lo festiu Fes-lo laborable Dimecres Àpats setmanals Hauries d'activar el JavaScript al teu navegador per poder emprar el sistema de '%s' populars segons únics primers 