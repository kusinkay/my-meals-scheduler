/// <reference path='../../vendor/offtic/wpcommons/src/assets/js/wpcommons.d.ts' />
/// <reference path='../../vendor/offtic/wpcommons/src/typescript/jquery.d.ts' />

module Offtic.MyMealsScheduler {


    export class WeekView extends Offtic.WPCommons.View {
        courses: Array<object>;

        available: Array<object>;
        selected: Array<object> = new Array<object>();

        ingredients: Object = {};

        days: Array<string> = ['', 'monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'];

        constructor() {
            super();
        }

        setViewName() {
            this.viewName = 'Week';
        }

        declareListeners() {

            var self = this;

            self.start();

            /**
             * add alt text for header rollover */
            jQuery( '.weekdayh' ).each( function() {
                if ( !jQuery( this ).hasClass( 'weekend' ) ) {
                    jQuery( this ).attr( 'title', ' ' + self.text( 'week_turn2holiday' ) );
                }
            } );

            /**
             * Turn a work day into holiday
             * */
            jQuery( '.weekdayh' ).off().on( 'click', function() {
                var now = 'holiday';
                var then = 'workday';
                if ( jQuery( this ).hasClass( 'workday' ) ) {
                    now = 'workday';
                    then = 'holiday';
                }
                if ( !jQuery( this ).hasClass( 'weekend' ) ) {
                    jQuery( this ).removeClass( now );
                    jQuery( this ).addClass( then );
                    var turn = 'week_turn2' + now;
                    jQuery( this ).attr( 'title', self.text( turn ) );

                    //take day from the first button
                    var day = jQuery( '.button:eq(0)', jQuery( this ).parent() ).attr( 'data-day' );
                    for ( var meal = 0; meal <= 1; meal++ ) {
                        self.update_meal( meal, parseInt( day ), false );
                    }
                }
            } );

            jQuery( '#reload-week-meals' ).click( function() {
                self.start();
            } );

            jQuery( '#close-meal' ).click( function() {
                self.close_meal();
            } )
        }

        private start() {
            var self = this;
            this.showLoading();
            var params: any = {}
            this.backofficeCall( "omms_get_meals", params, function( json ) {

                /** 
                 * set identifiers */
                for ( var i = 0; i < json.length; i++ ) {
                    json[i].id = i + 1;
                }
                self.courses = json;
                self.ingredients = {};
                var index = 0;
                self.available = Offtic.WPCommons.Utils.rand( self.courses );
                for ( var meal = 0; meal <= 1; meal++ ) {
                    for ( var day = 0; day <= 6; day++ ) {

                        self.assign( meal, day );

                    } //dies
                }//apats
                self.refresh();
                jQuery( '[data-cell]' ).off().on( 'click', function() {
                    /*
                    var parts = jQuery( this ).attr( 'data-cell' ).split( '|' );
                    var apat = parseInt( parts[0] );
                    var dia = parseInt( parts[1] );
                    jQuery( '#apat .apat' ).html(( apat == 0 ? 'dinar' : 'sopar' ) + '' );
                    jQuery( '#apat .dia' ).html( self.dies[dia] );
                    jQuery( '#apat button' ).attr( 'data-apat', apat );
                    jQuery( '#apat button' ).attr( 'data-dia', dia );
                    jQuery( '.reveal-overlay' ).show();
                    jQuery( '.reveal' ).show();
                    */
                } );
                if ( navigator == null || navigator.clipboard == undefined ) {
                    jQuery( '#copy-ingredients' ).hide();
                }
                if ( typeof params.loaded == 'function' ) {
                    params.loaded();
                }
            }, function() {
                /*error*/
            }, function() {
                /*complete*/
                self.hideLoading();
            } );
        }

        /**
         * Close meal functions dialog box
         */
        private close_meal() {
            jQuery( '.reveal-overlay' ).hide();
            jQuery( '.reveal' ).hide();
        }

        /**
         * See next available course
         */
        private next() {
            return this.available[this.available.length - 1];
        }

        private find( query: object, use?: boolean ): any {
            for ( var i = 0; i < this.available.length; i++ ) {
                var course = this.available[i];
                var found = true;
                for ( var attr of Object.keys( query ) ) {
                    if ( query.hasOwnProperty( attr ) ) {
                        var check = ( query[attr] == course[attr] ) || ( query[attr] == null ) && !course.hasOwnProperty( attr );
                        if ( Array.isArray( query[attr] ) ) {
                            check = false;
                            for ( var values = 0; values < query[attr].length; values++ ) {
                                check = check || ( course[attr] == query[attr][values] ) || ( query[attr][values] == null ) && !course.hasOwnProperty( attr );
                            }
                        }
                        found = found && check;
                    }
                }
                if ( found ) {
                    var result = this.available[i];
                    if ( use ) {
                        result = this.available.splice( i, 1 )[0];
                    }
                    console.debug( 'found (' + JSON.stringify( query ) + ')' );
                    console.debug( result );
                    return result;
                }
            }
            console.error( 'Not found (' + JSON.stringify( query ) + ')' );
            return false;
        }

        private target( meal: number, day: number ) {
            return '#meals tbody tr:eq(' + day + ') td:eq(' + ( meal + 1 ) + ')';
        }

        private empty( meal: number, day: number ) {
            var self = this;
            var target = this.target( meal, day );
            jQuery( target ).html( '' );

            this.add_button( target, {
                label: self.text( 'week_create_meal' ),
                class: 'create-courses',
                data: {
                    meal: meal,
                    day: day
                }
            } );

            jQuery( '.create-courses' ).off().on( 'click', function() {
                var meal = parseInt( jQuery( this ).attr( 'data-meal' ) );
                var day = parseInt( jQuery( this ).attr( 'data-day' ) );
                self.assign( meal, day );
            } )
        }

        private add_button( target, values: object ) {
            var cellbutton = true;
            var tag = 'span';
            if ( values['tag'] != null ) {
                tag = values['tag'];
                cellbutton = false;
            }
            var content = '&nbsp;';
            if ( values['content'] != null ) {
                content = values['content'];
            }
            var button = jQuery( '<' + tag + '>' );
            jQuery( button ).attr( 'title', values['label'] );
            jQuery( button ).addClass( values['class'] );
            if ( cellbutton ) {
                jQuery( button ).addClass( 'button' );
            }
            jQuery( button ).html( content );
            for ( var key of Object.keys( values['data'] ) ) {
                jQuery( button ).attr( 'data-' + key, values['data'][key] );
            }
            jQuery( target ).prepend( button );
        }

        /**
         * 
         * @param meal (0: lunch, 1: dinner)
         * @param day ( 1-7: monday-sunday )
         * @param id course identifier, pick the last one is empty
         */
        private use( meal: number, day: number, id?: string, add?: boolean ) {
            var self = this;
            if ( add == null ) {
                add = false;
            }
            var course: any;
            if ( id == null ) {
                console.debug( 'using last one' );
                course = this.available.splice( -1, 1 )[0];
            } else {
                course = this.find( { id: id }, true );
            }
            if ( course != false ) {
                course.emprat = {
                    apat: meal,
                    dia: day
                };
                var variation = '';
                if ( course.variacio != null ) {
                    course.variacio = Offtic.WPCommons.Utils.rand( course.variacio );
                    variation = course.variacio[0].nom;
                }
                var target = self.target( meal, day );
                var tpublic = '';
                if ( course.public != null ) {
                    tpublic = ( course.public != null && course.public == 'adult' ? '(a)' : '(n)' );
                }
                var ordre = '';
                if ( course.ordre != null ) {
                    ordre = course.ordre + ( course.ordre == 1 ? 'r' : 'n' ) + ': '
                }
                var caption = ordre + tpublic + course.titol + ' ' + variation;
                var dcourse = jQuery( '<div>' );
                jQuery( dcourse ).attr( 'class', 'course' );
                jQuery( dcourse ).attr( 'data-course-id', course.id );
                jQuery( dcourse ).html( caption );

                this.add_button( dcourse, {
                    //content: 'x',
                    tag: 'span',
                    label: self.text( 'week_remove_course' ),
                    class: 'remove-course',
                    data: {
                        meal: meal,
                        day: day,
                        id: course.id
                    }
                } );

                if ( !add ) {
                    jQuery( target ).html( '' );
                }
                jQuery( target ).append( dcourse );
                //jQuery( target ).html(( add ? jQuery( target ).html() + '<br>' : '<br>' ) + caption );
                jQuery( target ).attr( 'data-cell', day + '|' + meal )

                this.selected.push( course );
                if ( jQuery( '.button', jQuery( target ) ).length == 0 ) {
                    this.add_button( target, {
                        label: self.text( 'week_add_course' ),
                        class: 'select-course',
                        data: {
                            meal: meal,
                            day: day
                        }
                    } );

                    this.add_button( target, {
                        label: self.text( 'week_change_meal' ),
                        class: 'change-courses',
                        data: {
                            meal: meal,
                            day: day
                        }
                    } );

                    this.add_button( target, {
                        label: self.text( 'week_remove_meal' ),
                        class: 'remove-courses',
                        data: {
                            meal: meal,
                            day: day
                        }
                    } );
                }
            } else {
                console.warn( 'course not found: ' + id );
            }
        }

        private ingredient( ingredient: any ) {
            if ( !this.ingredients.hasOwnProperty( ingredient.nom ) ) {
                ingredient.quantitat = 1;
                this.ingredients[ingredient.nom] = ingredient;
            } else {
                this.ingredients[ingredient.nom].quantitat++;
            }
        }
        /**
         * 
         * @param recalculate based on selected courses
         */
        private list_ingredients( recalculate?: boolean ) {
            var self = this;
            if ( recalculate == null ) {
                recalculate = true;
            }
            if ( recalculate ) {
                this.ingredients = {};
                for ( var i = 0; i < this.selected.length; i++ ) {
                    var course: any = this.selected[i];
                    if ( course.variacio != null ) {
                        var ingredients = course.variacio[0].ingredients;
                        if ( ingredients != null ) {
                            for ( var j = 0; j < ingredients.length; j++ ) {
                                this.ingredient( ingredients[j] );
                            }
                        }
                    }
                    for ( var k = 0; k < course.ingredients.length; k++ ) {
                        var ingredient = course.ingredients[k];
                        this.ingredient( ingredient );
                    }
                }
            }
            jQuery( '#ingredients' ).html( '' );
            var qr = '';
            for ( var key of Object.keys( this.ingredients ).sort() ) {
                var ingredient = this.ingredients[key];
                var div = jQuery( '<div>' );
                jQuery( div ).html( ingredient.quantitat + ' x ' + ingredient.nom );
                qr += ingredient.nom + '\n';
                jQuery( div ).attr( 'class', 'ingredient graella bloc-4' );
                var delbtn = jQuery( '<span>' );
                jQuery( delbtn ).attr( 'class', 'delete-ingredient' );
                jQuery( delbtn ).attr( 'title', self.text( 'week_remove_ingredient' ) );
                jQuery( delbtn ).attr( 'data-del-ingredient', key );
                jQuery( div ).prepend( delbtn );
                jQuery( '#ingredients' ).append( div );
            }

            this.update_qr( qr );

            jQuery( '[data-del-ingredient]' ).off().on( 'click', function() {
                delete self.ingredients[jQuery( this ).attr( 'data-del-ingredient' )];
                self.list_ingredients( false );
            } );
        }

        private list_cookings() {
            var values = {};
            var totals = 0;
            for ( var i = 0; i < this.selected.length; i++ ) {
                var course: any = this.selected[i];
                var cooking = null;
                if ( course.coccio != null ) {
                    cooking = course.coccio;
                }
                if ( course.variacio != null ) {
                    cooking = course.variacio[0].coccio;
                }
                if ( cooking != null ) {
                    if ( values[cooking.tipus] == null ) {
                        values[cooking.tipus] = 1;
                    } else {
                        values[cooking.tipus]++;
                    }
                    totals++;
                }
            }//for
            var html = '';
            for ( var key of Object.keys( values ) ) {
                html += Math.round( values[key] / totals * 100 ) + '% - ' + key + ':  ' + values[key] + '<br>';
            }
            jQuery( '#coccions' ).html( html );
            console.debug( values );
        }

        private available_summary() {
            var values = {
                apat: {},
                public: {},
                ordre: {},
                restriccio: {}
            };
            for ( var i = 0; i < this.available.length; i++ ) {
                var course: any = this.available[i];
                var meal = 'indiferent';
                if ( course.apat != null ) {
                    meal = course.apat;
                }
                values.apat[meal] = ( values.apat[meal] == null ? 1 : values.apat[meal] + 1 );

                var tpublic = 'indiferent';
                if ( course.public != null ) {
                    tpublic = course.public;
                }
                values.public[tpublic] = ( values.public[tpublic] == null ? 1 : values.public[tpublic] + 1 );

                var order = 'únics';
                if ( course.ordre != null ) {
                    order = ( course.ordre == 1 ? 'primers' : 'segons' );
                }
                values.ordre[order] = ( values.ordre[order] == null ? 1 : values.ordre[order] + 1 );

                var restriction = 'resta';
                if ( course.restriccio != null ) {
                    restriction = ( course.restriccio == 'weekend' ? 'holiday' : course.restriccio );
                }
                values.restriccio[restriction] = ( values.restriccio[restriction] == null ? 1 : values.restriccio[restriction] + 1 );
            }
            var tpublic = '';
            for ( var key of Object.keys( values.public ) ) {
                tpublic += values.public[key] + ' ' + key + '<br>';
            }
            jQuery( '#courses-guests' ).html( tpublic );

            var meal = '';
            for ( var key of Object.keys( values.apat ) ) {
                meal += values.apat[key] + ' ' + key + '<br>';
            }
            jQuery( '#courses-meal' ).html( meal );

            var order = '';
            for ( var key of Object.keys( values.ordre ) ) {
                order += values.ordre[key] + ' ' + key + '<br>';
            }
            jQuery( '#courses-order' ).html( order );

            var restriction = '';
            for ( var key of Object.keys( values.restriccio ) ) {
                restriction += values.restriccio[key] + ' ' + key + '<br>';
            }
            jQuery( '#courses-restrinction' ).html( restriction );
            console.debug( values );
        }

        private refresh() {
            var self = this;
            this.list_ingredients();
            this.list_cookings();
            this.available_summary();


            jQuery( '.change-courses, .remove-courses' ).off().on( 'click', function() {
                var meal = parseInt( jQuery( this ).attr( 'data-meal' ) );
                var day = parseInt( jQuery( this ).attr( 'data-day' ) );
                self.update_meal( meal, day, jQuery( this ).hasClass( 'remove-courses' ) );
            } );

            jQuery( '.remove-course' ).off().on( 'click', function() {
                var meal = parseInt( jQuery( this ).attr( 'data-meal' ) );
                var day = parseInt( jQuery( this ).attr( 'data-day' ) );
                var id = parseInt( jQuery( this ).attr( 'data-id' ) );
                var pos = self.free( meal, day, id );
                jQuery( '[data-course-id=' + id + ']' ).remove();
                self.recover( pos );
                self.refresh();
            } );

            jQuery( '.select-course' ).off().on( 'click', function() {
                var meal = parseInt( jQuery( this ).attr( 'data-meal' ) );
                var day = parseInt( jQuery( this ).attr( 'data-day' ) );
                var data = {};
                for ( var i = 0; i < self.available.length; i++ ) {
                    var course: any = self.available[i];
                    var key = self.text( 'week_sole_courses' ) + ' ' + self.text( 'week_4all_guest_types' );
                    if ( course.ordre != null ) {
                        key = ( course.ordre == 1 ? self.text( 'week_starters' ) : self.text( 'week_main_courses' ) );
                    } else {
                        if ( course.public != null ) {
                            key = self.text( 'week_sole_courses' ) + ' ' + ( course.public );
                        }
                    }
                    if ( data[key] == null ) {
                        data[key] = new Array<object>();
                    }
                    data[key].push( { id: course.id, titol: course.titol } );
                }
                var target = self.target( meal, day );
                if ( jQuery( '.course-select', jQuery( target ) ).length == 0 ) {
                    var select = jQuery( '<select>' );
                    jQuery( select ).attr( 'class', 'course-select' );
                    jQuery( target ).prepend( select );
                } else {
                    jQuery( target + ' .course-select' ).html( '' );
                    jQuery( target + ' .course-select' ).show();
                }
                jQuery( target + ' .course-select' ).append( jQuery( '<option>' ) );
                for ( var key of Object.keys( data ) ) {
                    var optgroup = jQuery( '<optgroup>' );
                    jQuery( optgroup ).attr( 'label', key );

                    data[key].sort( function( a, b ) {
                        if ( a.titol > b.titol ) {
                            return 1;
                        } else if ( a.titol < b.titol ) {
                            return -1;
                        }
                        return 0;
                    } );
                    for ( var i = 0; i < data[key].length; i++ ) {
                        var course = data[key][i];
                        var option = jQuery( '<option>' );
                        jQuery( option ).val( course.id );
                        jQuery( option ).html( course.titol );
                        jQuery( optgroup ).append( option );
                    }

                    jQuery( target + ' .course-select' ).append( optgroup );
                }

                jQuery( '.course-select' ).off().on( 'change', function() {
                    jQuery( this ).hide();
                    self.use( meal, day, jQuery( this ).val(), true );
                    self.refresh();
                } );
            } );

            jQuery( '#copy-ingredients' ).off().on( 'click', function() {
                document.execCommand( "copy" );

                /* Get the text field */
                var copyText: any = document.getElementById( "msg" );

                /* Select the text field */
                copyText.select();
                copyText.setSelectionRange( 0, 99999 ); /* For mobile devices */
                if ( navigator != null && navigator.clipboard != undefined ) {
                    navigator.clipboard.writeText( copyText.value ).then(
                        function() {
                            Offtic.WPCommons.success( self.text( 'week_copied_ingredients' ) );
                        },
                        function() {
                            Offtic.WPCommons.alert( self.text( 'week_copied_ingred_fail' ) );
                        }
                    );
                } else {
                    /* Copy the text inside the text field */
                    //document.execCommand( "copy" );
                    Offtic.WPCommons.alert( self.text( 'week_copy_not_available' ) );
                }
            } );
        }

        private update_qr( qr: string ) {
            jQuery( '#msg' ).val( qr );
            /*update_qrcode();*/
        }

        /**
         * Removes current assignation and tries to assign another from available
         * @param meal
         * @param day
         */
        private assign( meal: number, day: number ) {
            var course: any = false;
            var query: any = {
                apat: ( meal == 0 ? ['dinar', null] : ['sopar', null] )
            };
            if ( jQuery( '#meals tbody tr:eq(' + day + ') td:eq(0)' ).hasClass( 'holiday' ) ) {
                query.restriccio = 'capdesetmana';
            } else {
                query.restriccio = null;
            }
            course = this.find( query );
            if ( course != false ) {
                this.use( meal, day, course.id );
                /**
                 * complementary course */
                this.complementary( meal, day, course, query );
                /* complementary course */
                if ( course.ordre == null ) {
                    // starter/main variants are treated in complementary 
                    this.alternative( meal, day, course, query );
                }

            } else {
                console.warn( 'course not found (' + meal + ', ' + day + '): ' + JSON.stringify( query ) );
            }
            this.refresh();
        }

        /**
         * Finds  a complementary course, if gotten course is a starter or a main (not a sole one)
         * @param meal
         * @param day
         * @param course
         * @param query
         */
        private complementary( meal: number, day: number, course: any, query: any ) {
            if ( course.ordre != null ) {
                /*  find a complementary course */
                query.ordre = ( course.ordre == 1 ? 2 : 1 );
                course = this.find( query );
                if ( course != false ) {
                    this.use( meal, day, course.id, true );
                    this.alternative( meal, day, course, query );
                }
            }
        }

        /**
         * Finds an alternative course based in public (adult/child)
         * @param meal
         * @param day
         * @param course
         * @param query
         */
        private alternative( meal: number, day: number, course: any, query: any ) {
            if ( course.public != null ) {
                var sole = false;
                if ( course.ordre == null ) {
                    //we got a sole course
                    sole = true;
                    query.ordre = null;
                }
                query.public = ( course.public == 'adult' ? ( sole ? null : 'nen' ) : 'adult' );
                course = this.find( query );
                if ( course != false ) {
                    this.use( meal, day, course.id, true );
                }
            }
        }

        /**
         * Informs about position in selected of current meal
         * to recover it afterwards.
         * 
         * @param meal
         * @param day
         * @param id course identifier
         */
        private free( meal: number, day: number, id?: number ) {
            var result: Array<number> = new Array<number>();
            for ( var i = 0; i < this.selected.length; i++ ) {
                var course: any = this.selected[i];
                if ( course.emprat.apat == meal && course.emprat.dia == day ) {
                    if ( id == null || course.id == id ) {
                        result.push( i );
                    }
                }
            }
            return result;
        }

        private recover( pos: Array<number> ) {
            for ( var i = 0; i < pos.length; i++ ) {
                this.available = this.available.concat( this.selected.splice( pos[i], 1 ) );
            }
        }

        private update_meal( meal: number, day: number, del: boolean ) {
            var pos = this.free( meal, day );
            if ( !del ) {
                this.assign( meal, day );
            } else {
                this.empty( meal, day );
            }
            this.recover( pos );
            this.refresh();
            this.close_meal();
        }

        public test() {
            console.debug( this.courses );
        }
    }
}