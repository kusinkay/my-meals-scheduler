<?php
namespace Offtic\MyMealsScheduler\Modules;

use Offtic\wpcommons\Module;
use Offtic\wpcommons\Context;

class WeekModule extends Module
{
    public function __construct( Context $context ) {
        parent::__construct($context);
    }

    public function add_pages()
    {
        $this->add_page("week", "Week meals", "week-meals", "[omms_week]");
    }

    public function add_shortcodes()
    {
        if (function_exists("add_shortcode")){
            add_shortcode( "omms_week", array( $this, 'week' ) );
        }
    }
    
    function week($atts) {
        $this->add_template('week.php');
        $this->add_reveals();
        return $this->run( __FUNCTION__ );
    }
}

