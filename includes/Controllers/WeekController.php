<?php
namespace Offtic\MyMealsScheduler\Controllers;

use Offtic\wpcommons\Context;
use Offtic\wpcommons\Controller;

class WeekController extends Controller
{
    public function __construct( Context $context ) {
        $this->methods = array(
            'get_meals',
        );
        parent::__construct($context);
    }
    
    function get_meals() {
        $this->response( function( WeekController $controller ) {
            return json_decode($controller->context->get_filecontent('/assets/data/meals.json'), TRUE );
        });
    }
}

