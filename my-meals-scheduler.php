<?php
use Offtic\wpcommons\Context;
use Offtic\wpcommons\Logger;
use Offtic\MyMealsScheduler\Modules\WeekModule;
use Offtic\wpcommons\Settings;
use Offtic\wpcommons\Settings\Section;
use Offtic\wpcommons\Settings\Field;

/**
 * Plugin Name: My Meals Scheduler
 * Description: WordPress plugin to generate a week meals table based on the dishes that work on our family
 * Version: 0.0.0_BETA
 * Author: Juane Puig
 * Author URI: https://offtic.com
 * Text Domain: my-meals-scheduler
 */
defined ( 'ABSPATH' ) || exit ();

require_once 'vendor/autoload.php';

if (! defined ( 'OMMS_PLUGIN_NAME' )) {
    define ( 'OMMS_PLUGIN_NAME', 'my-meals-scheduler' );
}
if (! defined ( 'OMMS_PLUGIN_FILE' )) {
    define ( 'OMMS_PLUGIN_FILE', __FILE__ );
}

$context = new Context('omms', OMMS_PLUGIN_NAME, OMMS_PLUGIN_FILE, '\\Offtic\\MyMealsScheduler', __('My Meals Scheduler', OMMS_PLUGIN_NAME) );
$context->add_module('Week');

$settings = new Settings($context);
{
    $section = new Section(
        $context,
        'owpc_devel',
        __('Devel options', $context->plugin_name),
        __('Options for developers and debugers', $context->plugin_name),
        $context->plugin_name);
    {
//         $section->add_field(new Field(array(
//             'name' => 'logthreshold2',
//             'label' => __('Log level', $context->plugin_name),
//             'default' => '0',
//             'description' => __('To debug errors and review them in apache logs. It will show levels downwards the options.', $context->plugin_name),
//             'options' => array(
//                 '0' => 'DEBUG',
//                 '1' => 'WARNING',
//                 '2' => 'INFO',
//                 '3' => 'ERROR',
//                 '4' => 'FATAL',
//             )
//         )));
    }
    $settings->add_section($section);
}


add_action( 'init', array($context, 'init') );
add_action( 'plugins_loaded', array($context, 'load_translation_files') );

