/// <reference path="../../vendor/offtic/wpcommons/src/assets/js/wpcommons.d.ts" />
/// <reference path="../../vendor/offtic/wpcommons/src/typescript/jquery.d.ts" />
declare module Offtic.MyMealsScheduler {
    class WeekView extends Offtic.WPCommons.View {
        courses: Array<object>;
        available: Array<object>;
        selected: Array<object>;
        ingredients: Object;
        days: Array<string>;
        constructor();
        setViewName(): void;
        declareListeners(): void;
        private start();
        /**
         * Close meal functions dialog box
         */
        private close_meal();
        /**
         * See next available course
         */
        private next();
        private find(query, use?);
        private target(meal, day);
        private empty(meal, day);
        private add_button(target, values);
        /**
         *
         * @param meal (0: lunch, 1: dinner)
         * @param day ( 1-7: monday-sunday )
         * @param id course identifier, pick the last one is empty
         */
        private use(meal, day, id?, add?);
        private ingredient(ingredient);
        /**
         *
         * @param recalculate based on selected courses
         */
        private list_ingredients(recalculate?);
        private list_cookings();
        private available_summary();
        private refresh();
        private update_qr(qr);
        /**
         * Removes current assignation and tries to assign another from available
         * @param meal
         * @param day
         */
        private assign(meal, day);
        /**
         * Finds  a complementary course, if gotten course is a starter or a main (not a sole one)
         * @param meal
         * @param day
         * @param course
         * @param query
         */
        private complementary(meal, day, course, query);
        /**
         * Finds an alternative course based in public (adult/child)
         * @param meal
         * @param day
         * @param course
         * @param query
         */
        private alternative(meal, day, course, query);
        /**
         * Informs about position in selected of current meal
         * to recover it afterwards.
         *
         * @param meal
         * @param day
         * @param id course identifier
         */
        private free(meal, day, id?);
        private recover(pos);
        private update_meal(meal, day, del);
        test(): void;
    }
}
