<?php 
/**
 * View literal declaration to be addressed by javascript
 */
$class_ref->literals['week_create_meal'] =              __('Create meal', OMMS_PLUGIN_NAME);
$class_ref->literals['week_remove_course'] =            __('Remove course', OMMS_PLUGIN_NAME);
$class_ref->literals['week_add_course'] =               __('Add course', OMMS_PLUGIN_NAME);
$class_ref->literals['week_change_meal'] =              __('Change meal', OMMS_PLUGIN_NAME);
$class_ref->literals['week_remove_meal'] =              __('Remove meal', OMMS_PLUGIN_NAME);
$class_ref->literals['week_copied_ingredients'] =       __('Copied ingredients to clipboard text-formated', OMMS_PLUGIN_NAME);
$class_ref->literals['week_copied_ingred_fail'] =       __('Could not copy ingredients to clipboard', OMMS_PLUGIN_NAME);
$class_ref->literals['week_copy_not_available'] =       __('Clipboard copy is not available', OMMS_PLUGIN_NAME);
$class_ref->literals['week_starters'] =                 __('starters', OMMS_PLUGIN_NAME);
$class_ref->literals['week_main_courses'] =             __('main courses', OMMS_PLUGIN_NAME);
$class_ref->literals['week_sole_courses'] =             __('sole courses', OMMS_PLUGIN_NAME);
$class_ref->literals['week_4all_guest_types'] =         __('for all guest types', OMMS_PLUGIN_NAME);
$class_ref->literals['week_turn2holiday'] =             __('Turn it holiday', OMMS_PLUGIN_NAME);
$class_ref->literals['week_turn2workday'] =             __('Turn it work day', OMMS_PLUGIN_NAME);
$class_ref->literals['week_remove_ingredient'] =        __('Remove ingredient', OMMS_PLUGIN_NAME);

?>
<noscript>
	<h2>
	<?php echo __('JavaScript disabled', OMMS_PLUGIN_NAME);?>
	</h2>
	<?php 
	//translators: it will embed the title of the module
	echo sprintf(__("You need to enable JavaScript in your browser so you can use '%s' system.", OMMS_PLUGIN_NAME), __("Week Meals", OMMS_PLUGIN_NAME));
	?>
</noscript>
<div class="view" data-view>
  <div id="week-table" class="face is-active" data-face>
	<button id="reload-week-meals">
		<span title="Canviar àpat" class="button">&nbsp;</span>
		<?php echo __('Refresh', OMMS_PLUGIN_NAME);?>
	</button>
	<table id="meals" border="1">
		<thead>
			<tr>
				<th><?php echo __('Meal', OMMS_PLUGIN_NAME);?></th>
				<th><?php echo __('Lunch', OMMS_PLUGIN_NAME);?></th>
				<th><?php echo __('Dinner', OMMS_PLUGIN_NAME);?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="weekdayh workday"><b><?php echo __('Monday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="weekdayh workday"><b><?php echo __('Tuesday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="weekdayh workday"><b><?php echo __('Wednesday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="weekdayh workday"><b><?php echo __('Thursday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="weekdayh workday"><b><?php echo __('Friday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="weekdayh weekend holiday"><b><?php echo __('Saturday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
			<tr>
				<td class="weekdayh weekend holiday"><b><?php echo __('Sunday', OMMS_PLUGIN_NAME);?></b></td>
				<td></td>
				<td></td>
			</tr>
		</tbody>
	</table>
	
	<div class="graella bloc-12">
		<h2><?php echo __('Ingredients', OMMS_PLUGIN_NAME);?></h2>
		<button id="copy-ingredients"><?php echo __('Copy ingredients to clipboard', OMMS_PLUGIN_NAME);?></button>
		<div id="qr"></div>
		<div id="ingredients">
			
		</div>
	</div>
	
	<div class="graella bloc-6">
		<h2><?php echo __('Available dishes', OMMS_PLUGIN_NAME);?></h2>
		<h3><?php echo __('By dinner guest type', OMMS_PLUGIN_NAME);?></h3>
		<div id="courses-guests">
		</div>
		<h3><?php echo __('By meal', OMMS_PLUGIN_NAME);?></h3>
		<div id="courses-meal">
		</div>
		<h3><?php echo __('By dish order', OMMS_PLUGIN_NAME);?></h3>
		<div id="courses-order">
		</div>
		<h3><?php echo __('By day type', OMMS_PLUGIN_NAME);?></h3>
		<div id="courses-restrinction">
		</div>
	</div>
	
	<div class="graella bloc-6">
		<h2><?php echo __('By cooking type', OMMS_PLUGIN_NAME);?></h2>
		<div id="coccions">
		</div>
	</div>
	
	<form name ="qrForm" class=view-form style="display:none">
		<textarea rows="20" cols="20" name="msg" id="msg"></textarea>
		<input type="text" name="t" value="0"/>
		<input type="text" name="e" value="M"/>
		<input type="text" name="m" value="Byte"/>
		<input type="text" name="mb" value="UTF-8"/>
	</form>
	
	<div class="reveal-overlay">
		<div class="reveal" id="apat">
			<span class="apat">{apat}</span> de <span class="dia">{dia}</span>
			<ul>
				<li><span class="elimina-plats" title="Elimina plat(s)">&nbsp</span></li>
				<li><button class="canvia-plats">Canviar plat(s)</button></li>
				
				<li><button id="close-meal">Tancar</button></li>
			</ul>
			
		</div>
	</div>
  </div><!-- //face -->
</div><!-- view -->